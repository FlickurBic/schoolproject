﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAlgoritm
{
    class Program
    {
        static void Main(string[] args)
        {
            string user;
            int console = 1;
            bool forkYes = false; //перепенные для подсчёта шагов до развилок
            int fork1 = 0;
            int fork2 = 0;
            int fork3 = 0;
            int forkLevel = 1;
            user = Console.ReadLine();


            //oъявляем массив
            int[][] Labirint1 = new int[13][];
            //в каждую строку добавляем столбцы
            Labirint1[0] = new int[23];
            Labirint1[1] = new int[23];
            Labirint1[2] = new int[23];
            Labirint1[3] = new int[23];
            Labirint1[4] = new int[23];
            Labirint1[5] = new int[23];
            Labirint1[6] = new int[23];
            Labirint1[7] = new int[23];
            Labirint1[8] = new int[23];
            Labirint1[9] = new int[23];
            Labirint1[10] = new int[23];
            Labirint1[11] = new int[23];
            Labirint1[12] = new int[23];
            //заполняем строки
            //строка 0
            Labirint1[0][0] = 1;
            Labirint1[0][1] = 1;
            Labirint1[0][2] = 1;
            Labirint1[0][3] = 1;
            Labirint1[0][4] = 1;
            Labirint1[0][5] = 1;
            Labirint1[0][6] = 1;
            Labirint1[0][7] = 1;
            Labirint1[0][8] = 1;
            Labirint1[0][9] = 1;
            Labirint1[0][10] = 1;
            Labirint1[0][11] = 1;
            Labirint1[0][12] = 1;
            Labirint1[0][13] = 1;
            Labirint1[0][14] = 1;
            Labirint1[0][15] = 1;
            Labirint1[0][16] = 1;
            Labirint1[0][17] = 1;
            Labirint1[0][18] = 1;
            Labirint1[0][19] = 1;
            Labirint1[0][20] = 1;
            Labirint1[0][21] = 1;
            Labirint1[0][22] = 1;
            //строка 1
            Labirint1[1][0] = 1;
            Labirint1[1][1] = 0;
            Labirint1[1][2] = 0;
            Labirint1[1][3] = 0;
            Labirint1[1][4] = 0;
            Labirint1[1][5] = 0;
            Labirint1[1][6] = 0;
            Labirint1[1][7] = 0;
            Labirint1[1][8] = 0;
            Labirint1[1][9] = 2;
            Labirint1[1][10] = 1;
            Labirint1[1][11] = 11;
            Labirint1[1][12] = 1;
            Labirint1[1][13] = 0;
            Labirint1[1][14] = 0;
            Labirint1[1][15] = 0;
            Labirint1[1][16] = 0;
            Labirint1[1][17] = 0;
            Labirint1[1][18] = 1;
            Labirint1[1][19] = 0;
            Labirint1[1][20] = 0;
            Labirint1[1][21] = 0;
            Labirint1[1][22] = 1;
            //строка 2
            Labirint1[2][0] = 1;
            Labirint1[2][1] = 0;
            Labirint1[2][2] = 1;
            Labirint1[2][3] = 1;
            Labirint1[2][4] = 1;
            Labirint1[2][5] = 1; //стенка
            Labirint1[2][6] = 1;
            Labirint1[2][7] = 1;
            Labirint1[2][8] = 1;
            Labirint1[2][9] = 1;
            Labirint1[2][10] = 1;
            Labirint1[2][11] = 0;
            Labirint1[2][12] = 1;
            Labirint1[2][13] = 1;
            Labirint1[2][14] = 1;
            Labirint1[2][15] = 0;
            Labirint1[2][16] = 1;
            Labirint1[2][17] = 0;
            Labirint1[2][18] = 1;
            Labirint1[2][19] = 1;
            Labirint1[2][20] = 1;
            Labirint1[2][21] = 0;
            Labirint1[2][22] = 1;
            //строка 3
            Labirint1[3][0] = 1;
            Labirint1[3][1] = 0;
            Labirint1[3][2] = 0;
            Labirint1[3][3] = 0;
            Labirint1[3][4] = 0;
            Labirint1[3][5] = 0;
            Labirint1[3][6] = 0;
            Labirint1[3][7] = 0;
            Labirint1[3][8] = 0;
            Labirint1[3][9] = 0;
            Labirint1[3][10] = 0;
            Labirint1[3][11] = 0;
            Labirint1[3][12] = 0;
            Labirint1[3][13] = 3;
            Labirint1[3][14] = 1;
            Labirint1[3][15] = 0;
            Labirint1[3][16] = 1;
            Labirint1[3][17] = 0;
            Labirint1[3][18] = 1;
            Labirint1[3][19] = 0;
            Labirint1[3][20] = 0;
            Labirint1[3][21] = 0;
            Labirint1[3][22] = 1;
            //строка 4
            Labirint1[4][0] = 1;
            Labirint1[4][1] = 1;
            Labirint1[4][2] = 1;
            Labirint1[4][3] = 1;
            Labirint1[4][4] = 1;
            Labirint1[4][5] = 1; 
            Labirint1[4][6] = 1;
            Labirint1[4][7] = 1;
            Labirint1[4][8] = 1;
            Labirint1[4][9] = 1;
            Labirint1[4][10] = 1;
            Labirint1[4][11] = 0;
            Labirint1[4][12] = 1;
            Labirint1[4][13] = 0;
            Labirint1[4][14] = 1;
            Labirint1[4][15] = 0;
            Labirint1[4][16] = 1;
            Labirint1[4][17] = 0;
            Labirint1[4][18] = 1;
            Labirint1[4][19] = 0;
            Labirint1[4][20] = 1;
            Labirint1[4][21] = 1;
            Labirint1[4][22] = 1;
            //строка 5
            Labirint1[5][0] = 1;
            Labirint1[5][1] = 0;
            Labirint1[5][2] = 0;
            Labirint1[5][3] = 0;
            Labirint1[5][4] = 0;
            Labirint1[5][5] = 0;
            Labirint1[5][6] = 0;
            Labirint1[5][7] = 0;
            Labirint1[5][8] = 0;
            Labirint1[5][9] = 0;
            Labirint1[5][10] = 1;
            Labirint1[5][11] = 0;
            Labirint1[5][12] = 1;
            Labirint1[5][13] = 0;
            Labirint1[5][14] = 0;
            Labirint1[5][15] = 0;
            Labirint1[5][16] = 1;
            Labirint1[5][17] = 0;
            Labirint1[5][18] = 0;
            Labirint1[5][19] = 0;
            Labirint1[5][20] = 1;
            Labirint1[5][21] = 5;
            Labirint1[5][22] = 1;
            //строка 6
            Labirint1[6][0] = 1;
            Labirint1[6][1] = 1;
            Labirint1[6][2] = 1;
            Labirint1[6][3] = 1;
            Labirint1[6][4] = 1;
            Labirint1[6][5] = 1;
            Labirint1[6][6] = 1;
            Labirint1[6][7] = 1;
            Labirint1[6][8] = 1;
            Labirint1[6][9] = 1;
            Labirint1[6][10] = 1;
            Labirint1[6][11] = 9;
            Labirint1[6][12] = 1;
            Labirint1[6][13] = 1;
            Labirint1[6][14] = 1;
            Labirint1[6][15] = 1;
            Labirint1[6][16] = 1;
            Labirint1[6][17] = 1;
            Labirint1[6][18] = 1;
            Labirint1[6][19] = 1;
            Labirint1[6][20] = 1;
            Labirint1[6][21] = 0;
            Labirint1[6][22] = 1;
            //строка 7
            Labirint1[7][0] = 1;
            Labirint1[7][1] = 0;
            Labirint1[7][2] = 0;
            Labirint1[7][3] = 0;
            Labirint1[7][4] = 0;
            Labirint1[7][5] = 0;
            Labirint1[7][6] = 0;
            Labirint1[7][7] = 0;
            Labirint1[7][8] = 0;
            Labirint1[7][9] = 0;
            Labirint1[7][10] = 8;
            Labirint1[7][11] = 0;
            Labirint1[7][12] = 6;
            Labirint1[7][13] = 0;
            Labirint1[7][14] = 1;
            Labirint1[7][15] = 0;
            Labirint1[7][16] = 1;
            Labirint1[7][17] = 0;
            Labirint1[7][18] = 0;
            Labirint1[7][19] = 0;
            Labirint1[7][20] = 0;
            Labirint1[7][21] = 0;
            Labirint1[7][22] = 1;
            //строка 8
            Labirint1[8][0] = 1;
            Labirint1[8][1] = 0;
            Labirint1[8][2] = 1;
            Labirint1[8][3] = 1;
            Labirint1[8][4] = 1;
            Labirint1[8][5] = 1;
            Labirint1[8][6] = 1;
            Labirint1[8][7] = 1;
            Labirint1[8][8] = 1;
            Labirint1[8][9] = 1;
            Labirint1[8][10] = 1;
            Labirint1[8][11] = 1;
            Labirint1[8][12] = 1;
            Labirint1[8][13] = 0;
            Labirint1[8][14] = 1;
            Labirint1[8][15] = 0;
            Labirint1[8][16] = 1;
            Labirint1[8][17] = 1;
            Labirint1[8][18] = 1;
            Labirint1[8][19] = 0;
            Labirint1[8][20] = 1;
            Labirint1[8][21] = 0;
            Labirint1[8][22] = 1;
            //строка 9 
            Labirint1[9][0] = 1;
            Labirint1[9][1] = 0;
            Labirint1[9][2] = 1;
            Labirint1[9][3] = 4;
            Labirint1[9][4] = 1;
            Labirint1[9][5] = 0;
            Labirint1[9][6] = 0;
            Labirint1[9][7] = 0;
            Labirint1[9][8] = 0;
            Labirint1[9][9] = 0;
            Labirint1[9][10] = 1;
            Labirint1[9][11] = 0;
            Labirint1[9][12] = 0;
            Labirint1[9][13] = 0;
            Labirint1[9][14] = 0;
            Labirint1[9][15] = 0;
            Labirint1[9][16] = 0;
            Labirint1[9][17] = 0;
            Labirint1[9][18] = 1;
            Labirint1[9][19] = 0;
            Labirint1[9][20] = 1;
            Labirint1[9][21] = 0;
            Labirint1[9][22] = 1;
            //строка 10
            Labirint1[10][0] = 1;
            Labirint1[10][1] = 0;
            Labirint1[10][2] = 1;
            Labirint1[10][3] = 0;
            Labirint1[10][4] = 1;
            Labirint1[10][5] = 1;
            Labirint1[10][6] = 1;
            Labirint1[10][7] = 1;
            Labirint1[10][8] = 1;
            Labirint1[10][9] = 0;
            Labirint1[10][10] = 1;
            Labirint1[10][11] = 1;
            Labirint1[10][12] = 1;
            Labirint1[10][13] = 1;
            Labirint1[10][14] = 1;
            Labirint1[10][15] = 0;
            Labirint1[10][16] = 1;
            Labirint1[10][17] = 1;
            Labirint1[10][18] = 1;
            Labirint1[10][19] = 1;
            Labirint1[10][20] = 1;
            Labirint1[10][21] = 0;
            Labirint1[10][22] = 1;
            //строка 11
            Labirint1[11][0] = 1;
            Labirint1[11][1] = 0;
            Labirint1[11][2] = 0;
            Labirint1[11][3] = 0;
            Labirint1[11][4] = 1;
            Labirint1[11][5] = 0;
            Labirint1[11][6] = 0;
            Labirint1[11][7] = 0;
            Labirint1[11][8] = 1;
            Labirint1[11][9] = 0;
            Labirint1[11][10] = 0;
            Labirint1[11][11] = 10;
            Labirint1[11][12] = 7;
            Labirint1[11][13] = 0;
            Labirint1[11][14] = 0;
            Labirint1[11][15] = 0;
            Labirint1[11][16] = 0;
            Labirint1[11][17] = 0;
            Labirint1[11][18] = 0;
            Labirint1[11][19] = 0;
            Labirint1[11][20] = 0;
            Labirint1[11][21] = 0;
            Labirint1[11][22] = 1;
            //строка 12
            Labirint1[12][0] = 1;
            Labirint1[12][1] = 1;
            Labirint1[12][2] = 1;
            Labirint1[12][3] = 1;
            Labirint1[12][4] = 1;
            Labirint1[12][5] = 1;
            Labirint1[12][6] = 1;
            Labirint1[12][7] = 1;
            Labirint1[12][8] = 1;
            Labirint1[12][9] = 1;
            Labirint1[12][10] = 1;
            Labirint1[12][11] = 1;
            Labirint1[12][12] = 1;
            Labirint1[12][13] = 1;
            Labirint1[12][14] = 1;
            Labirint1[12][15] = 1;
            Labirint1[12][16] = 1;
            Labirint1[12][17] = 1;
            Labirint1[12][18] = 1;
            Labirint1[12][19] = 1;
            Labirint1[12][20] = 1;
            Labirint1[12][21] = 1;
            Labirint1[12][22] = 1;

            while (console != 0)
            {
                switch (user)
                {
                    case "start":
                        //алгоритм
                        int NumerDeltaI = 1;
                        int NumerDeltaJ = 1;
                        int LabelI = 0;
                        int LabelJ = 0;
                        int Standart = 0;
                        int[][] LeverLevel = new int[13][];
                        LeverLevel[0] = new int[22];
                        LeverLevel[1] = new int[22];
                        LeverLevel[2] = new int[22];
                        LeverLevel[3] = new int[22];
                        LeverLevel[4] = new int[22];
                        LeverLevel[5] = new int[22];
                        LeverLevel[6] = new int[22];
                        LeverLevel[7] = new int[22];
                        LeverLevel[8] = new int[22];
                        LeverLevel[9] = new int[22];
                        LeverLevel[10] = new int[22];
                        LeverLevel[11] = new int[22];
                        LeverLevel[12] = new int[22];

                        string direction = " "; // a - LabelI-- ; b - LabelJ-- ; c  - LabelI++ ; d - LabelJ++
                        int sс = 1;
                        while (sс != 0) //Этап 1(вычёркивание тупиков) + проверка на рычаги
                        {
                            sс = 0;
                            for (int i = 2; i < 12; i++)
                            {
                                for (int j = 2; j < 22; j++)
                                {
                                    if (Labirint1[i][j] == 0 & Labirint1[i - 1][j] == 1 & Labirint1[i][j - 1] == 1 & Labirint1[i + 1][j] == 1)
                                    {
                                        Labirint1[i][j] = 1;
                                        sс++;
                                    }
                                    else if (Labirint1[i][j] == 0 & Labirint1[i][j - 1] == 1 & Labirint1[i + 1][j] == 1 & Labirint1[i][j + 1] == 1)
                                    {
                                        Labirint1[i][j] = 1;
                                        sс++;
                                    }
                                    else if (Labirint1[i][j] == 0 & Labirint1[i + 1][j] == 1 & Labirint1[i][j + 1] == 1 & Labirint1[i - 1][j] == 1)
                                    {
                                        Labirint1[i][j] = 1;
                                        sс++;
                                    }
                                    else if (Labirint1[i][j] == 0 & Labirint1[i][j + 1] == 1 & Labirint1[i - 1][j] == 1 & Labirint1[i][j - 1] == 1)
                                    {
                                        Labirint1[i][j] = 1;
                                        sс++;
                                    }
                                }

                            }
                        }

                        for (int i = 0; i < 13; i++) {
                            for (int j = 0; j < 23; j++) {
                                if (Labirint1[i][j] == 11)
                                {
                                    LabelI = i;
                                    LabelJ = j;
                                }
                                else if (Labirint1[i][j] == 2)
                                {
                                    LeverLevel[i][j] = 11;
                                }
                                else if (Labirint1[i][j] == 3)
                                {
                                    LeverLevel[i][j] = 20;
                                }
                                else if (Labirint1[i][j] == 4)
                                {
                                    LeverLevel[i][j] = 31;
                                }
                                else if (Labirint1[i][j] == 5) {
                                    LeverLevel[i][j] = 40;
                                }
                            }
                        }
                        bool stop = false;
                        Console.WriteLine("Координаты для прохождения лабиринта:");
                        StringBuilder kord = new StringBuilder();
                        user = "";
                        while (stop != true) {
                        Start:
                            if (Labirint1[LabelI - 1][LabelJ] == 0) // если в окружении проходы(0)
                            {
                                if (forkYes == true)
                                {
                                    switch (forkLevel)
                                    {
                                        case 131:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelI--;
                                            fork1++;

                                            break;
                                        case 132:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelI--;
                                            fork2++;

                                            break;
                                        case 133:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelI--;
                                            fork3++;

                                            break;
                                    }
                                } else {
                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                    Standart = Labirint1[LabelI][LabelJ];
                                    Labirint1[LabelI][LabelJ] = 12;
                                    NumerDeltaI = LabelI;
                                    NumerDeltaJ = LabelJ;
                                    LabelI--;
                                    if (direction != "a")
                                    {
                                        kord.Append("[");
                                        kord.Append(Convert.ToString(LabelI));
                                        kord.Append("] [");
                                        kord.Append(Convert.ToString(LabelJ));
                                        kord.AppendLine("] ;");
                                    }
                                    direction = "a";
                                }
                            }
                            else if (Labirint1[LabelI][LabelJ - 1] == 0 & Labirint1[LabelI + 1][LabelJ] != 0 & Labirint1[LabelI - 1][LabelJ] != 0)
                            {
                                if (forkYes == true)
                                {
                                    switch (forkLevel) {
                                        case 131:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelJ--;
                                            fork1++;

                                            break;
                                        case 132:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelJ--;
                                            fork2++;

                                            break;
                                        case 133:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelJ--;
                                            fork3++;

                                            break;
                                    }
                                }
                                else {
                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                    Standart = Labirint1[LabelI][LabelJ];
                                    Labirint1[LabelI][LabelJ] = 12;
                                    NumerDeltaI = LabelI;
                                    NumerDeltaJ = LabelJ;
                                    LabelJ--;
                                    if (direction != "b")
                                    {
                                        kord.Append("[");
                                        kord.Append(Convert.ToString(LabelI));
                                        kord.Append("] [");
                                        kord.Append(Convert.ToString(LabelJ));
                                        kord.AppendLine("] ;");
                                    }
                                    direction = "b";
                                }
                            }
                            else if (Labirint1[LabelI + 1][LabelJ] == 0 & Labirint1[LabelI][LabelJ - 1] == 1 & Labirint1[LabelI][LabelJ + 1] == 1)
                            {
                                if (forkYes == true)
                                {
                                    switch (forkLevel)
                                    {
                                        case 131:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelI++;
                                            fork1++;

                                            break;
                                        case 132:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelI++;
                                            fork2++;

                                            break;
                                        case 133:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelI++;
                                            fork3++;

                                            break;
                                    }
                                }
                                else
                                {
                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                    Standart = Labirint1[LabelI][LabelJ];
                                    Labirint1[LabelI][LabelJ] = 12;
                                    NumerDeltaI = LabelI;
                                    NumerDeltaJ = LabelJ;
                                    LabelI++;
                                    if (direction != "c")
                                    {
                                        kord.Append("[");
                                        kord.Append(Convert.ToString(LabelI));
                                        kord.Append("] [");
                                        kord.Append(Convert.ToString(LabelJ));
                                        kord.AppendLine("] ;");
                                    }
                                    direction = "c";
                                }
                            }
                            else if (Labirint1[LabelI][LabelJ + 1] == 0 & Labirint1[LabelI + 1][LabelJ] != 0 & Labirint1[LabelI - 1][LabelJ] != 0)
                            {
                                if (forkYes == true)
                                {
                                    switch (forkLevel)
                                    {
                                        case 131:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelJ++;
                                            fork1++;

                                            break;
                                        case 132:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelJ++;
                                            fork2++;

                                            break;
                                        case 133:
                                            Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                            Standart = Labirint1[LabelI][LabelJ];
                                            Labirint1[LabelI][LabelJ] = 14;
                                            NumerDeltaI = LabelI;
                                            NumerDeltaJ = LabelJ;
                                            LabelJ++;
                                            fork3++;

                                            break;
                                    }
                                }
                                else
                                {
                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                    Standart = Labirint1[LabelI][LabelJ];
                                    Labirint1[LabelI][LabelJ] = 12;
                                    NumerDeltaI = LabelI;
                                    NumerDeltaJ = LabelJ;
                                    LabelJ++;
                                    if (direction != "d")
                                    {
                                        kord.Append("[");
                                        kord.Append(Convert.ToString(LabelI));
                                        kord.Append("] [");
                                        kord.Append(Convert.ToString(LabelJ));
                                        kord.AppendLine("] ;");
                                    }
                                    direction = "d";
                                }
                            }
                            else if (Labirint1[LabelI - 1][LabelJ] == 10) //проверяем на выход
                            {
                                stop = true;
                                LabelI--;
                                Console.WriteLine(kord);
                            }
                            else if (Labirint1[LabelI + 1][LabelJ] == 10) //проверяем на выход
                            {
                                stop = true;
                                LabelI++;
                                Console.WriteLine(kord);
                            }
                            else if (Labirint1[LabelI][LabelJ - 1] == 10) //проверяем на выход
                            {
                                stop = true;
                                LabelJ--;
                                Console.WriteLine(kord);
                            }
                            else if (Labirint1[LabelI][LabelJ + 1] == 10) //проверяем на выход
                            {
                                stop = true;
                                LabelJ++;
                                Console.WriteLine(kord);
                            } //описываем рычаг
                            else if (Labirint1[LabelI - 1][LabelJ] == 2 | Labirint1[LabelI - 1][LabelJ] == 3 | Labirint1[LabelI - 1][LabelJ] == 4 | Labirint1[LabelI - 1][LabelJ] == 5)
                            {
                                LabelI--;
                                if (Analistic(LabelI, LabelJ, LeverLevel) == true)
                                {
                                    
                                    if (LeverLevel[LabelI][LabelJ] == 10)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 8)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 20)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 9)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 30)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 6)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }
                                        Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                        Standart = Labirint1[LabelI][LabelJ];
                                        Labirint1[LabelI][LabelJ] = 12;
                                        NumerDeltaI = LabelI;
                                        NumerDeltaJ = LabelJ;
                                        LabelI++;
                                        
                                            kord.Append("[");
                                            kord.Append(Convert.ToString(LabelI));
                                            kord.Append("] [");
                                            kord.Append(Convert.ToString(LabelJ));
                                            kord.AppendLine("] ;");
                                        
                                        direction = "c";
                                        Labirint1[LabelI][LabelJ] = 1;
                                        Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                        Standart = Labirint1[LabelI][LabelJ];
                                        Labirint1[LabelI][LabelJ] = 12;
                                        NumerDeltaI = LabelI;
                                        NumerDeltaJ = LabelJ;
                                        LabelJ--;
                                        
                                            kord.Append("[");
                                            kord.Append(Convert.ToString(LabelI));
                                            kord.Append("] [");
                                            kord.Append(Convert.ToString(LabelJ));
                                            kord.AppendLine("] ;");
                                        
                                        direction = "b";
                                        //что-то странное
                                        Labirint1[LabelI][LabelJ] = 1;
                                        Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                        Standart = Labirint1[LabelI][LabelJ];
                                        Labirint1[LabelI][LabelJ] = 12;
                                        NumerDeltaI = LabelI;
                                        NumerDeltaJ = LabelJ;
                                        LabelJ--;

                                        kord.Append("[");
                                        kord.Append(Convert.ToString(LabelI));
                                        kord.Append("] [");
                                        kord.Append(Convert.ToString(LabelJ));
                                        kord.AppendLine("] ;");

                                        direction = "b";
                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 40)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 7)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }

                                }
                                else
                                {
                                    Labirint1[LabelI][LabelJ] = 0;
                                    LabelI++;
                                }
                            }
                            else if (Labirint1[LabelI + 1][LabelJ] == 2 | Labirint1[LabelI + 1][LabelJ] == 3 | Labirint1[LabelI + 1][LabelJ] == 4 | Labirint1[LabelI + 1][LabelJ] == 5)
                            {
                                LabelI++;
                                if (Analistic(LabelI, LabelJ, LeverLevel) == true)
                                {
                                    if (LeverLevel[LabelI][LabelJ] == 10)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI--;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 8)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 20)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI--;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 9)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 30)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI--;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 6)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 40)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelI--;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 7)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }

                                }
                                else
                                {
                                    Labirint1[LabelI][LabelJ] = 0;
                                    LabelI--;
                                }
                            }
                            else if (Labirint1[LabelI][LabelJ - 1] == 2 | Labirint1[LabelI][LabelJ - 1] == 3 | Labirint1[LabelI][LabelJ - 1] == 4 | Labirint1[LabelI][LabelJ - 1] == 5)
                            {
                                LabelJ--;
                                if (Analistic(LabelI, LabelJ, LeverLevel) == true)
                                {
                                    if (LeverLevel[LabelI][LabelJ] == 10)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelJ++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 8)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 20)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelJ++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 9)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 30)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelJ++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 6)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }
                                    else if (LeverLevel[LabelI][LabelJ] == 40)
                                    {
                                        Labirint1[LabelI][LabelJ] = 15;
                                        LabelJ++;
                                        for (int s = 0; s < 13; s++)
                                        {
                                            for (int u = 0; u < 23; u++)
                                            {
                                                if (Labirint1[s][u] == 7)
                                                {
                                                    Labirint1[s][u] = 0;
                                                    break;
                                                }
                                            }
                                        }

                                    }

                                }
                                else
                                {
                                    Labirint1[LabelI][LabelJ] = 0;
                                    LabelJ++;
                                }
                                
                            }
                            else if (Labirint1[LabelI][LabelJ + 1] == 2 | Labirint1[LabelI][LabelJ + 1] == 3 | Labirint1[LabelI][LabelJ + 1] == 4 | Labirint1[LabelI][LabelJ + 1] == 5)
                            {
                                LabelJ++;
                                if (forkYes == true)
                                {
                                    if (Analistic(LabelI, LabelJ, LeverLevel) == true)
                                    {
                                        switch (forkLevel)
                                        {
                                            case 131:
                                                forkLevel = 132;

                                                for (int hh = 0; hh < 13; hh++)
                                                {
                                                    for (int hj = 0; hj < 23; hj++)
                                                    {
                                                        if (Labirint1[hh][hj] == 13)
                                                        {
                                                            LabelI = hh;
                                                            LabelJ = hj;
                                                            LabelI++;
                                                            goto Start;
                                                            break;
                                                        }
                                                    }
                                                }
                                                break;
                                            case 132:
                                                forkLevel = 133;
                                                for (int hh = 0; hh < 13; hh++)
                                                {
                                                    for (int hj = 0; hj < 23; hj++)
                                                    {
                                                        if (Labirint1[hh][hj] == 13)
                                                        {
                                                            LabelI = hh;
                                                            LabelJ = hj;
                                                            LabelJ++;
                                                            goto Start;
                                                            break;
                                                        }
                                                    }
                                                }
                                                break;
                                            case 133:
                                                Labirint1[1][9] = 2;
                                                LeverLevel[1][9] = 11;
                                                for (int hh = 0; hh < 13; hh++)
                                                {
                                                    for (int hj = 0; hj < 23; hj++)
                                                    {
                                                        if (Labirint1[hh][hj] == 13)
                                                        {
                                                            LabelI = hh;
                                                            LabelJ = hj;
                                                            break;
                                                        }
                                                    }
                                                }
                                                forkYes = false;
                                                Labirint1[LabelI][LabelJ] = 0;
                                                if (fork1 > fork2 & fork2 < fork3)
                                                { //идём по fork2

                                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                    Standart = Labirint1[LabelI][LabelJ];
                                                    Labirint1[LabelI][LabelJ] = 12;
                                                    NumerDeltaI = LabelI;
                                                    NumerDeltaJ = LabelJ;
                                                    LabelJ--;
                                                    if (direction != "b")
                                                    {
                                                        kord.Append("[");
                                                        kord.Append(Convert.ToString(LabelI));
                                                        kord.Append("] [");
                                                        kord.Append(Convert.ToString(LabelJ));
                                                        kord.AppendLine("] ;");
                                                    }
                                                    direction = "b";

                                                }
                                                else if (fork1 < fork2 & fork1 < fork3)
                                                { //идём по fork1
                                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                    Standart = Labirint1[LabelI][LabelJ];
                                                    Labirint1[LabelI][LabelJ] = 12;
                                                    NumerDeltaI = LabelI;
                                                    NumerDeltaJ = LabelJ;
                                                    LabelI++;
                                                    if (direction != "c")
                                                    {
                                                        kord.Append("[");
                                                        kord.Append(Convert.ToString(LabelI));
                                                        kord.Append("] [");
                                                        kord.Append(Convert.ToString(LabelJ));
                                                        kord.AppendLine("] ;");
                                                    }
                                                    direction = "c";
                                                }
                                                else if (fork3 < fork1 & fork3 < fork2)
                                                { //идём по fork3
                                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                    Standart = Labirint1[LabelI][LabelJ];
                                                    Labirint1[LabelI][LabelJ] = 12;
                                                    NumerDeltaI = LabelI;
                                                    NumerDeltaJ = LabelJ;
                                                    LabelJ++;
                                                    if (direction != "d")
                                                    {
                                                        kord.Append("[");
                                                        kord.Append(Convert.ToString(LabelI));
                                                        kord.Append("] [");
                                                        kord.Append(Convert.ToString(LabelJ));
                                                        kord.AppendLine("] ;");
                                                    }
                                                    direction = "d";
                                                }
                                                break;
                                        }
                                        
                                    } else
                                    { //если рычаг бесполезен
                                        switch (forkLevel)
                                        {
                                            case 131:
                                                forkLevel = 132;
                                                fork1 = 999999999;
                                                for (int hh = 0; hh < 13; hh++)
                                                {
                                                    for (int hj = 0; hj < 23; hj++)
                                                    {
                                                        if (Labirint1[hh][hj] == 13)
                                                        {
                                                            LabelI = hh;
                                                            LabelJ = hj;
                                                            break;
                                                        }
                                                    }
                                                }
                                                break;
                                            case 132:
                                                forkLevel = 133;
                                                fork2 = 999999999;
                                                for (int hh = 0; hh < 13; hh++)
                                                {
                                                    for (int hj = 0; hj < 23; hj++)
                                                    {
                                                        if (Labirint1[hh][hj] == 13)
                                                        {
                                                            LabelI = hh;
                                                            LabelJ = hj;
                                                            break;
                                                        }
                                                    }
                                                }
                                                break;
                                            case 133:
                                                fork3 = 999999999;
                                                Labirint1[1][9] = 2;
                                                LeverLevel[1][9] = 11;
                                                for (int hh = 0; hh < 13; hh++)
                                                {
                                                    for (int hj = 0; hj < 23; hj++)
                                                    {
                                                        if (Labirint1[hh][hj] == 13)
                                                        {
                                                            LabelI = hh;
                                                            LabelJ = hj;
                                                            break;
                                                        }
                                                    }
                                                }
                                                forkYes = false;
                                                if (fork1 > fork2 & fork2 < fork3)
                                                { //идём по fork2
                                                    LabelI++;
                                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                    Standart = Labirint1[LabelI][LabelJ];
                                                    Labirint1[LabelI][LabelJ] = 12;
                                                    NumerDeltaI = LabelI;
                                                    NumerDeltaJ = LabelJ;
                                                    LabelI++;
                                                    
                                                        kord.Append("[");
                                                        kord.Append(Convert.ToString(LabelI));
                                                        kord.Append("] [");
                                                        kord.Append(Convert.ToString(LabelJ));
                                                        kord.AppendLine("] ;");
                                                    
                                                    direction = "c";

                                                }
                                                else if (fork1 < fork2 & fork1 < fork3)
                                                { //идём по fork1
                                                    LabelJ--;
                                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                    Standart = Labirint1[LabelI][LabelJ];
                                                    Labirint1[LabelI][LabelJ] = 12;
                                                    NumerDeltaI = LabelI;
                                                    NumerDeltaJ = LabelJ;
                                                    LabelJ--;
                                                    
                                                        kord.Append("[");
                                                        kord.Append(Convert.ToString(LabelI));
                                                        kord.Append("] [");
                                                        kord.Append(Convert.ToString(LabelJ));
                                                        kord.AppendLine("] ;");
                                                    
                                                    direction = "b";
                                                    goto Start;
                                                }
                                                else if (fork3 < fork1 & fork3 < fork2)
                                                { //идём по fork3
                                                    LabelJ++;
                                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                    Standart = Labirint1[LabelI][LabelJ];
                                                    Labirint1[LabelI][LabelJ] = 12;
                                                    NumerDeltaI = LabelI;
                                                    NumerDeltaJ = LabelJ;
                                                    LabelJ++;
                                                    
                                                        kord.Append("[");
                                                        kord.Append(Convert.ToString(LabelI));
                                                        kord.Append("] [");
                                                        kord.Append(Convert.ToString(LabelJ));
                                                        kord.AppendLine("] ;");
                                                    
                                                    direction = "d";
                                                }
                                                break;
                                        }
                                    }
                                }
                                else
                                {
                                    
                                    if (Analistic(LabelI, LabelJ, LeverLevel) == true)
                                    {
                                        
                                        if (LeverLevel[LabelI][LabelJ] == 10)
                                        {
                                            Labirint1[LabelI][LabelJ] = 0;
                                            LabelJ--;
                                            for (int s = 0; s < 13; s++)
                                            {
                                                for (int u = 0; u < 23; u++)
                                                {
                                                    if (Labirint1[s][u] == 8)
                                                    {
                                                        Labirint1[s][u] = 0;
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                        else if (LeverLevel[LabelI][LabelJ] == 20)
                                        {
                                            Labirint1[LabelI][LabelJ] = 0;
                                            LabelJ--;
                                            for (int s = 0; s < 13; s++)
                                            {
                                                for (int u = 0; u < 23; u++)
                                                {
                                                    if (Labirint1[s][u] == 9)
                                                    {
                                                        Labirint1[s][u] = 0;
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                        else if (LeverLevel[LabelI][LabelJ] == 30)
                                        {
                                            Labirint1[LabelI][LabelJ] = 0;
                                            LabelJ--;
                                            for (int s = 0; s < 13; s++)
                                            {
                                                for (int u = 0; u < 23; u++)
                                                {
                                                    if (Labirint1[s][u] == 6)
                                                    {
                                                        Labirint1[s][u] = 0;
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                        else if (LeverLevel[LabelI][LabelJ] == 40)
                                        {
                                            Labirint1[LabelI][LabelJ] = 0;
                                            LabelJ--;
                                            for (int s = 0; s < 13; s++)
                                            {
                                                for (int u = 0; u < 23; u++)
                                                {
                                                    if (Labirint1[s][u] == 7)
                                                    {
                                                        Labirint1[s][u] = 0;
                                                        break;
                                                    }
                                                }
                                            }

                                        }
                                        Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                        Standart = Labirint1[LabelI][LabelJ];
                                        Labirint1[LabelI][LabelJ] = 12;
                                        NumerDeltaI = LabelI;
                                        NumerDeltaJ = LabelJ;
                                        LabelJ--;
                                        
                                            kord.Append("[");
                                            kord.Append(Convert.ToString(LabelI));
                                            kord.Append("] [");
                                            kord.Append(Convert.ToString(LabelJ));
                                            kord.AppendLine("] ;");
                                        
                                        direction = "b";
                                        Labirint1[LabelI][LabelJ] = 1;
                                        Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                        Standart = Labirint1[LabelI][LabelJ];
                                        Labirint1[LabelI][LabelJ] = 12;
                                        NumerDeltaI = LabelI;
                                        NumerDeltaJ = LabelJ;
                                        LabelJ--;
                                        if (direction != "b")
                                        {
                                            kord.Append("[");
                                            kord.Append(Convert.ToString(LabelI));
                                            kord.Append("] [");
                                            kord.Append(Convert.ToString(LabelJ));
                                            kord.AppendLine("] ;");
                                        }
                                        direction = "b";
                                    }
                                    
                                    else
                                    {
                                        Labirint1[LabelI][LabelJ] = 0;
                                        LabelJ--;                    //   | |
                                    }                               // === * ===
                                                                    //описываем развилки // 4 дороги      | |
                                }
                            }
                            else if (Labirint1[LabelI + 1][LabelJ] == 9)
                            {
                                
                                for (int sss = 0; sss < 12; sss++)
                                {
                                    for(int jjj = 0; jjj < 23; jjj++)
                                    {
                                        if(Labirint1[sss][jjj] == 3)
                                        {
                                            if (LeverLevel[sss][jjj] == 20)
                                            {
                                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                Standart = Labirint1[LabelI][LabelJ];
                                                Labirint1[LabelI][LabelJ] = 12;
                                                NumerDeltaI = LabelI;
                                                NumerDeltaJ = LabelJ;
                                                LabelI++;
                                                if (direction != "c")
                                                {
                                                    kord.Append("[");
                                                    kord.Append(Convert.ToString(LabelI));
                                                    kord.Append("] [");
                                                    kord.Append(Convert.ToString(LabelJ));
                                                    kord.AppendLine("] ;");
                                                }
                                                direction = "c";
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Labirint1[LabelI][LabelJ - 1] == 8)
                            {

                                for (int sss = 0; sss < 12; sss++)
                                {
                                    for (int jjj = 0; jjj < 23; jjj++)
                                    {
                                        if (Labirint1[sss][jjj] == 2)
                                        {
                                            if (LeverLevel[sss][jjj] == 10)
                                            {
                                                forkYes = false;
                                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                Standart = Labirint1[LabelI][LabelJ];
                                                Labirint1[LabelI][LabelJ] = 12;
                                                NumerDeltaI = LabelI;
                                                NumerDeltaJ = LabelJ;
                                                LabelJ--;
                                                if (direction != "b")
                                                {
                                                    kord.Append("[");
                                                    kord.Append(Convert.ToString(LabelI));
                                                    kord.Append("] [");
                                                    kord.Append(Convert.ToString(LabelJ));
                                                    kord.AppendLine("] ;");
                                                }
                                                direction = "b";
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Labirint1[LabelI + 1][LabelJ] != 1 & Labirint1[LabelI][LabelJ + 1] != 1 & fork1 != 0)
                            {

                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                Standart = Labirint1[LabelI][LabelJ];
                                Labirint1[LabelI][LabelJ] = 12;
                                NumerDeltaI = LabelI;
                                NumerDeltaJ = LabelJ;
                                LabelI++;
                                if (direction != "c")
                                {
                                    kord.Append("[");
                                    kord.Append(Convert.ToString(LabelI));
                                    kord.Append("] [");
                                    kord.Append(Convert.ToString(LabelJ));
                                    kord.AppendLine("] ;");
                                }
                                direction = "c";

                            }
                            else if(Labirint1[LabelI + 1][LabelJ] != 1 & Labirint1[LabelI][LabelJ - 1] != 1 & fork1 != 0)
                            {
                                
                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                Standart = Labirint1[LabelI][LabelJ];
                                Labirint1[LabelI][LabelJ] = 12;
                                NumerDeltaI = LabelI;
                                NumerDeltaJ = LabelJ;
                                LabelI++;
                                
                                    kord.Append("[");
                                    kord.Append(Convert.ToString(LabelI));
                                    kord.Append("] [");
                                    kord.Append(Convert.ToString(LabelJ));
                                    kord.AppendLine("] ;");
                                
                                direction = "c";
                            }
                            else if (Labirint1[LabelI][LabelJ - 1] == 7)
                            {
                                for (int sss = 0; sss < 12; sss++)
                                {
                                    for (int jjj = 0; jjj < 23; jjj++)
                                    {
                                        if (Labirint1[sss][jjj] == 2)
                                        {
                                            if (LeverLevel[sss][jjj] == 10)
                                            {
                                                forkYes = false;
                                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                Standart = Labirint1[LabelI][LabelJ];
                                                Labirint1[LabelI][LabelJ] = 12;
                                                NumerDeltaI = LabelI;
                                                NumerDeltaJ = LabelJ;
                                                LabelJ--;
                                                if (direction != "b")
                                                {
                                                    kord.Append("[");
                                                    kord.Append(Convert.ToString(LabelI));
                                                    kord.Append("] [");
                                                    kord.Append(Convert.ToString(LabelJ));
                                                    kord.AppendLine("] ;");
                                                }
                                                direction = "b";
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Labirint1[LabelI - 1][LabelJ] != 1 & Labirint1[LabelI][LabelJ - 1] != 1 & Labirint1[LabelI][LabelJ + 1] != 1 & Labirint1[LabelI + 1][LabelJ] != 1) {

                                if (direction == "a")
                                { //мы пришли сверху

                                }
                                else if (direction == "b")
                                { //мы пришли справа
                                }
                                else if (direction == "c")
                                { //мы пришли снизу
                                    forkLevel = 131;
                                    fork1 = 0;
                                    fork2 = 0;
                                    fork3 = 0;
                                    Labirint1[LabelI][LabelJ] = 13;
                                    LabelJ--;

                                }
                                else if (direction == "d")
                                {  //мы пришли слева

                                }
                                forkYes = true;
                                goto Start;

                            } else if (Labirint1[LabelI][LabelJ - 1] != 1 & Labirint1[LabelI][LabelJ + 1] != 1) {

                                if (direction == "a")
                                { //мы пришли сверху

                                }
                                else if (direction == "b")
                                { //мы пришли справа
                                }
                                else if (direction == "c")
                                { //мы пришли снизу


                                }
                                else if (direction == "d")
                                {  //мы пришли слева
                                    Labirint1[LabelI][LabelJ] = 0;
                                    Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                    Standart = Labirint1[LabelI][LabelJ];
                                    Labirint1[LabelI][LabelJ] = 12;
                                    NumerDeltaI = LabelI;
                                    NumerDeltaJ = LabelJ;
                                    LabelJ++;

                                    kord.Append("[");
                                    kord.Append(Convert.ToString(LabelI));
                                    kord.Append("] [");
                                    kord.Append(Convert.ToString(LabelJ));
                                    kord.AppendLine("] ;");

                                    direction = "d";
                                    forkYes = true;
                                    goto Start;
                                }

                            }
                            else //если алгоритмом не обработана ситуация
                            {
                                if (forkYes == true) //если тупик и мы в развилке
                                {
                                    Console.WriteLine("stop");
                                    Console.ReadLine();
                                    switch (forkLevel)
                                    {
                                        case 131:
                                            forkLevel = 132;
                                            fork1 = 999999999;
                                            for (int hh = 0; hh < 13; hh++)
                                            {
                                                for (int hj = 0; hj < 23; hj++)
                                                {
                                                    if (Labirint1[hh][hj] == 13)
                                                    {
                                                        LabelI = hh;
                                                        LabelJ = hj;
                                                        LabelI++;
                                                        break;
                                                    }
                                                }
                                            }
                                            break;
                                        case 132:
                                            forkLevel = 133;
                                            fork2 = 999999999;
                                            for (int hh = 0; hh < 13; hh++)
                                            {
                                                for (int hj = 0; hj < 23; hj++)
                                                {
                                                    if (Labirint1[hh][hj] == 13)
                                                    {
                                                        LabelI = hh;
                                                        LabelJ = hj;
                                                        LabelJ++;
                                                        break;
                                                    }
                                                }
                                            }
                                            break;
                                        case 133:
                                            fork3 = 999999999;
                                            for (int hh = 0; hh < 13; hh++)
                                            {
                                                for (int hj = 0; hj < 23; hj++)
                                                {
                                                    if (Labirint1[hh][hj] == 13)
                                                    {
                                                        LabelI = hh;
                                                        LabelJ = hj;
                                                        break;
                                                    }
                                                }
                                            }
                                            forkYes = false;
                                            Labirint1[LabelI][LabelJ] = 0;
                                            if (fork1 > fork2 & fork2 < fork3)
                                            { //идём по fork2

                                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                Standart = Labirint1[LabelI][LabelJ];
                                                Labirint1[LabelI][LabelJ] = 12;
                                                NumerDeltaI = LabelI;
                                                NumerDeltaJ = LabelJ;
                                                LabelJ--;
                                                if (direction != "b")
                                                {
                                                    kord.Append("[");
                                                    kord.Append(Convert.ToString(LabelI));
                                                    kord.Append("] [");
                                                    kord.Append(Convert.ToString(LabelJ));
                                                    kord.AppendLine("] ;");
                                                }
                                                direction = "b";

                                            }
                                            else if (fork1 < fork2 & fork1 < fork3)
                                            { //идём по fork1
                                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                Standart = Labirint1[LabelI][LabelJ];
                                                Labirint1[LabelI][LabelJ] = 12;
                                                NumerDeltaI = LabelI;
                                                NumerDeltaJ = LabelJ;
                                                LabelI++;
                                                if (direction != "c")
                                                {
                                                    kord.Append("[");
                                                    kord.Append(Convert.ToString(LabelI));
                                                    kord.Append("] [");
                                                    kord.Append(Convert.ToString(LabelJ));
                                                    kord.AppendLine("] ;");
                                                }
                                                direction = "c";
                                            }
                                            else if (fork3 < fork1 & fork3 < fork2)
                                            { //идём по fork3
                                                Labirint1[NumerDeltaI][NumerDeltaJ] = Standart;
                                                Standart = Labirint1[LabelI][LabelJ];
                                                Labirint1[LabelI][LabelJ] = 12;
                                                NumerDeltaI = LabelI;
                                                NumerDeltaJ = LabelJ;
                                                LabelJ++;
                                                if (direction != "d")
                                                {
                                                    kord.Append("[");
                                                    kord.Append(Convert.ToString(LabelI));
                                                    kord.Append("] [");
                                                    kord.Append(Convert.ToString(LabelJ));
                                                    kord.AppendLine("] ;");
                                                }
                                                direction = "d";
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                        //алгоритм
                        break;
                    case "quit":
                        console = 0;
                        break;
                    default:
                        Console.WriteLine("Неизвестная команда!");
                        user = Console.ReadLine();
                        break;
                }
            }
        }
        public static bool Analistic(int i, int j, int[][] LeverLevel) {
            if (LeverLevel[i][j] == 11)
            {
                LeverLevel[i][j] = 10;
                return true;
            }
            else if (LeverLevel[i][j] == 21)
            {
                LeverLevel[i][j] = 20;
                return true;
            }
            else if (LeverLevel[i][j] == 31)
            {
                LeverLevel[i][j] = 30;
                return true;
            }
            else if (LeverLevel[i][j] == 41)
            {
                LeverLevel[i][j] = 40;
                return true;
            }
            else {
                return false;
            }
        }
    }
}
